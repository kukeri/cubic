// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GoogleData
{
        public const string achievement_longest_played_time = "CgkIsP3h0oEVEAIQBg"; // <GPGSID>
        public const string achievement_max_levels = "CgkIsP3h0oEVEAIQAA"; // <GPGSID>
        public const string leaderboard_high_score = "CgkIsP3h0oEVEAIQAQ"; // <GPGSID>
        public const string achievement_highest_score = "CgkIsP3h0oEVEAIQAw"; // <GPGSID>
        public const string achievement_low_bullets = "CgkIsP3h0oEVEAIQBA"; // <GPGSID>
        public const string achievement_most_passed_levels = "CgkIsP3h0oEVEAIQBQ"; // <GPGSID>

}

