﻿using System;
using System.Collections;
using Assets.Scripts.PHP;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Json
{
    using UI.LevelSelector;

    [Serializable]
    public class Player
    {
        public string Name;
        public int Score;
        public int regularBullets;
    }


    public class ServerLogin : MonoBehaviour
    {
        public string userName;

        public Text myName;

        private int alreadyStarted;

        private Player player = new Player();

        void Start()
        {

#if UNITY_ANDROID


            GooglePlayGames.PlayGamesPlatform.Activate();
            //TODO move method here after done


            this.alreadyStarted = PlayerPrefs.GetInt("StartedAlready", this.alreadyStarted);

            //TODO add check for started game;
            //TODO fix classesf
            Social.localUser.Authenticate((success) =>
            {
                this.userName = Social.localUser.userName;

                if (success)
                {
                    this.myName.text = this.userName;
                    this.player.Name = this.userName;
                    WWWForm playerDataForm = new WWWForm();
                    playerDataForm.AddField("PlayerData", JsonUtility.ToJson(this.player));
                    WWW sendData = new WWW(Servers.addUserURL, playerDataForm);

                    this.StartCoroutine(this.SentPlayerData(sendData));
                }
                else
                {
                    this.myName.text = "You are out my friend";
                    Debug.Log("OMG you  are the first person who can read this.");
                }
            });


#endif


        }

        private IEnumerator SentPlayerData(WWW sendData)
        {

            yield return sendData;

            this.myName.text = sendData.text;
            int levelStartedd = PlayerPrefs.GetInt("StartedAlready", this.alreadyStarted);
            //TODO firstTime login


            string localLevelProgress;

            if (sendData.bytesDownloaded == 0)
            {
                localLevelProgress = PlayerPrefs.GetString("LevelsProgress");
            }
            else
            {
                localLevelProgress = sendData.text;
                PlayerPrefs.SetString("LevelsProgress", sendData.text);
            }


            JSONObject levelData = new JSONObject(localLevelProgress);

            LevelProgressStatic.levelProgress = levelData.ToIntDictionary();

        }
    }
}

