﻿using System;

namespace Assets.Scripts.Json
{
    using UnityEngine;


    [Serializable]
    public class Level
    {
        public string name;

        public int numberOfRegularMeteorites;
        public int numberOfFireMeteorites;
        public int numberOfIceMeteorites;
        public int numberOfHardMeteorites;
        public int numberOfSpinningMeteorites;

        public int numberOfNormalCrates;

        public float regularMeteoritTimer;
        public float fireMeteoritTimer;
        public float iceMeteoritTimer;
        public float hardMeteoritTimer;
        public float spiningMeteoritTimer;

        public float normalCrateTimer;

        public int numberOfRegularBullets;
        public int numberOfHomingBullets;
        public int numberOfRainPainBullets;
        public int timeForForceField;

        public int meteoriteDropCount;
        public int totalMeteoritCount;
        public int levelMaxScore;
    }

    public class LevelsSerialisation : MonoBehaviour
    {
        private string tutorialLevelData;
        private string level1Data;
        private string level2Data;
        private string level3Data;
        private string level4Data;
        private string level5Data;

        private Level tutorialLevel = new Level();
        private Level level1 = new Level();
        private Level level2 = new Level();
        private Level level3 = new Level();
        private Level level4 = new Level();
        private Level level5 = new Level();

        private int alreadyStarted;

        public void Start()
        {
            this.alreadyStarted = PlayerPrefs.GetInt("StartedAlready", this.alreadyStarted);

            //TODO For DataBase 


            //Removed if
            this.Level1Data();
            this.Level2Data();
            this.Level3Data();
            this.Level4Data();
            this.Level5Data();
            this.TutorialLevel();

            this.DataToJson();
            this.SaveInPlayerPrefs();

            PlayerPrefs.SetInt("StartedAlready", 0);


        }

        private void DataToJson()
        {
            this.tutorialLevelData = JsonUtility.ToJson(this.tutorialLevel);
            this.level1Data = JsonUtility.ToJson(this.level1);
            this.level2Data = JsonUtility.ToJson(this.level2);
            this.level3Data = JsonUtility.ToJson(this.level3);
            this.level4Data = JsonUtility.ToJson(this.level4);
            this.level5Data = JsonUtility.ToJson(this.level5);
        }

        private void SaveInPlayerPrefs()
        {
            PlayerPrefs.SetString(this.tutorialLevel.name, this.tutorialLevelData);
            PlayerPrefs.SetString(this.level1.name, this.level1Data);
            PlayerPrefs.SetString(this.level2.name, this.level2Data);
            PlayerPrefs.SetString(this.level3.name, this.level3Data);
            PlayerPrefs.SetString(this.level4.name, this.level4Data);
            PlayerPrefs.SetString(this.level5.name, this.level5Data);
        }

        private void TutorialLevel()
        {
            this.tutorialLevel.name = "TutorialLevel-Data";
            this.tutorialLevel.numberOfRegularMeteorites = 5;
            this.tutorialLevel.regularMeteoritTimer = 1.5f;
            this.tutorialLevel.numberOfRegularBullets = 200;
            this.tutorialLevel.totalMeteoritCount = 5;
            this.tutorialLevel.levelMaxScore = 50;
        }

        private void Level1Data()
        {
            this.level1.name = "Level1-Data";
            this.level1.numberOfRegularMeteorites = 10;
            this.level1.regularMeteoritTimer = 0.9f;
            this.level1.numberOfIceMeteorites = 2;
            this.level1.iceMeteoritTimer = 1.2f;
            this.level1.numberOfHardMeteorites = 2;
            this.level1.hardMeteoritTimer = 1.7f;
            this.level1.numberOfRegularBullets = 200;
            this.level1.totalMeteoritCount = 4;
            this.level1.levelMaxScore = 50;
            this.level1.normalCrateTimer = 5;
            this.level1.numberOfNormalCrates = 3;
        }

        private void Level2Data()
        {
            this.level2.name = "Level2-Data";
            this.level2.numberOfIceMeteorites = 5;
            this.level2.iceMeteoritTimer = 1.3f;
            this.level2.numberOfRegularBullets = 200;
            this.level2.totalMeteoritCount = 5;
            this.level2.levelMaxScore = 150;
            this.level2.normalCrateTimer = 2.5f;
        }

        private void Level3Data()
        {
            this.level3.name = "Level3-Data";
            this.level3.numberOfFireMeteorites = 30;
            this.level3.fireMeteoritTimer = 1f;
            this.level3.numberOfRegularBullets = 200;
            this.level3.totalMeteoritCount = 30;
            this.level3.levelMaxScore = 600;
            this.level3.normalCrateTimer = 5;
            this.level3.numberOfNormalCrates = 3;
        }

        private void Level4Data()
        {
            this.level4.name = "Level4-Data";
            this.level4.numberOfSpinningMeteorites = 30;
            this.level4.spiningMeteoritTimer = 0.9f;
            this.level4.numberOfRegularBullets = 200;
            this.level4.totalMeteoritCount = 30;
            this.level4.levelMaxScore = 1500;
            this.level4.normalCrateTimer = 5;
            this.level4.numberOfNormalCrates = 3;
        }

        private void Level5Data()
        {
            this.level5.name = "Level5-Data";
            this.level5.numberOfHardMeteorites = 30;
            this.level5.hardMeteoritTimer = 2f;
            this.level5.numberOfRegularBullets = 500;
            this.level5.totalMeteoritCount = 30;
            this.level5.levelMaxScore = 1200;
            this.level5.normalCrateTimer = 5;
            this.level5.numberOfNormalCrates = 3;
        }
    }

}
