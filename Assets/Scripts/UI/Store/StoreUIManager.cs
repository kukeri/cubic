﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoreUIManager : MonoBehaviour
{

    // Use this for initialization
    public Button SkinsButton;
    public Button CoinsButton;
    public Button BulletsButton;

    public GameObject SkinsScroll;
    public GameObject CoinsScroll;
    public GameObject BulletsScroll;

    void Start()
    {
        this.SkinsButton.targetGraphic.color = this.SkinsButton.colors.highlightedColor;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            this.BackToMenu();
        }
    }

    public void SelectSkins()
    {
        this.SkinsButton.targetGraphic.color = this.SkinsButton.colors.highlightedColor;
        this.BulletsButton.targetGraphic.color = this.BulletsButton.colors.normalColor;
        this.CoinsButton.targetGraphic.color = this.CoinsButton.colors.normalColor;
        this.SkinsScroll.SetActive(true);
        this.BulletsScroll.SetActive(false);
        this.CoinsScroll.SetActive(false);
    }

    public void SelectCoins()
    {
        this.SkinsButton.targetGraphic.color = this.SkinsButton.colors.normalColor;
        this.BulletsButton.targetGraphic.color = this.BulletsButton.colors.normalColor;
        this.CoinsButton.targetGraphic.color = this.CoinsButton.colors.highlightedColor;
        this.SkinsScroll.SetActive(false);
        this.BulletsScroll.SetActive(false);
        this.CoinsScroll.SetActive(true);
    }

    public void SelectBullets()
    {
        this.SkinsButton.targetGraphic.color = this.SkinsButton.colors.normalColor;
        this.BulletsButton.targetGraphic.color = this.BulletsButton.colors.highlightedColor;
        this.CoinsButton.targetGraphic.color = this.CoinsButton.colors.normalColor;
        this.SkinsScroll.SetActive(false);
        this.BulletsScroll.SetActive(true);
        this.CoinsScroll.SetActive(false);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(ScenesEnum.StartScene.ToString());
    }
}
