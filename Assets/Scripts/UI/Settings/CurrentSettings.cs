﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UI.Settings
{
    public static class CurrentSettings
    {
        public static bool isMusicMuted = PlayerPrefs.GetInt("IsMusicMuted", 0) == 1;
        public static bool isFXMuted = PlayerPrefs.GetInt("IsFXMuted", 0) == 1;
        public static float tiltSensitivity = PlayerPrefs.GetFloat("sensitivity", 0.5f);
        public static string controller = PlayerPrefs.GetString("PlayMode", "Buttons");
    }
}
