﻿using UnityEngine;
using System.Collections;
using System.Linq;
using Assets.Scripts.UI.Settings;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingUIManager : MonoBehaviour
{      
    public Dropdown dropDown;
    public Text dropDownLabel;
    public Toggle MusicToggle;
    public Toggle FXToggle;
    public GameObject CreditsPanel;

    public Slider sensitivity;
    public GameObject sensitivityLabel;

    void Start()
    {
        InitialiseCurrentSettings();
        int valueForPlaymode = dropDown.options.FindIndex(x => x.text == CurrentSettings.controller);
        dropDown.value = valueForPlaymode;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            GoBackToMenu();
        }
    }

    private void InitialiseCurrentSettings()
    {
        if (!CurrentSettings.isMusicMuted)
        {
            this.MusicToggle.isOn = false;
        }
        else
        {
            this.MusicToggle.isOn = true;
        }

        if (!CurrentSettings.isFXMuted)
        {
            this.FXToggle.isOn = false;
        }
        else
        {
            this.FXToggle.isOn = true;
        }

        this.dropDownLabel.text = CurrentSettings.controller;
        if (CurrentSettings.controller == "Tilt")
        {
            print(CurrentSettings.controller);
            this.sensitivity.gameObject.SetActive(true);         
            this.sensitivity.value = CurrentSettings.tiltSensitivity;
        }
    }

    public void SetCurrentPlaymode()
    {
        string currentMode = dropDown.captionText.text;
        if (currentMode == "Tilt")
        {
            this.sensitivity.gameObject.SetActive(true);
            this.sensitivityLabel.gameObject.SetActive(true);
        }
        else
        {
            this.sensitivityLabel.gameObject.SetActive(false);
            this.sensitivity.gameObject.SetActive(false);
        }
        CurrentSettings.controller = currentMode;
        PlayerPrefs.SetString("PlayMode", currentMode);
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(ScenesEnum.StartScene.ToString());
    }

    public void SetMusicState()
    {
        bool isMusicMuted = MusicToggle.isOn;
        if (isMusicMuted)
        {
            PlayerPrefs.SetInt("IsMusicMuted", 1);
        }
        else
        {
            PlayerPrefs.SetInt("IsMusicMuted", 0);
        }

        CurrentSettings.isMusicMuted = isMusicMuted;
    }

    public void SetFXSoundsState()
    {
        bool isFXMuted = FXToggle.isOn;
        if (isFXMuted)
        {
            PlayerPrefs.SetInt("IsFXMuted", 1);
        }
        else
        {
            PlayerPrefs.SetInt("IsFXMuted", 0);
        }

        CurrentSettings.isFXMuted = isFXMuted;
    }

    public void ShowCredits()
    {
        this.CreditsPanel.SetActive(!this.CreditsPanel.activeSelf);
    }

    public void OnSensitivityChange()
    {                                 
        float sensitivity = this.sensitivity.value;
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
        CurrentSettings.tiltSensitivity = sensitivity;
    }
}
