﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AchievementsUIScript : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            this.BackToMenu();
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(ScenesEnum.StartScene.ToString());
    }
}
