﻿using UnityEngine;
using System.Collections;

public class AchievementCollector : MonoBehaviour
{                                            
    public Animation piggyAnimation;

    public void CollectFromMeteorites()
    {
        string typeOfMeteorites = this.name;
        int meteoritesDestroyedOfCurrentType = PlayerPrefs.GetInt(string.Format("{0}-destroyed", typeOfMeteorites));
        int currentIndex = PlayerPrefs.GetInt(string.Format("{0}-achievement-index", typeOfMeteorites));
        int wantedMeteorites = PlayerPrefs.GetInt(string.Format("{0}-{1}", typeOfMeteorites, currentIndex));

        if (meteoritesDestroyedOfCurrentType >= wantedMeteorites)
        {
            piggyAnimation.Play();
        }
    }
}
