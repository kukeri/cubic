﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Cuby;

public class GameOver : MonoBehaviour
{
    private new SpriteRenderer renderer;

    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        renderer.enabled = false;
    }

    void Update()
    {
        GameOverState();
    }

    void GameOverState()
    {
        if (!CubyStaticData.IsAlive)
        {
            renderer.enabled = true;
        }
    }
}
