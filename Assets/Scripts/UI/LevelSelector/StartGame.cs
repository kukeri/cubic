﻿
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public GameObject loadingScreen;

    private IEnumerator LoadLevelAsync(ScenesEnum scene)
    {
        this.loadingScreen.SetActive(true);
        AsyncOperation async = SceneManager.LoadSceneAsync(scene.ToString());

        yield return async;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            this.BackToMenu();
        }
    }

    public void Level1()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.Level1));
    }

    public void Level2()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.Level2));

    }

    public void Level3()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.Level3));

    }

    public void Level4()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.Level4));
    }

    public void Level5()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.Level5));
    }

    public void TutorialLevel()
    {
        StartCoroutine(this.LoadLevelAsync(ScenesEnum.TutorialLevel));
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(ScenesEnum.StartScene.ToString());
    }
}
