﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.Json;
using Assets.Scripts.PHP;
using Assets.Scripts.UI.LevelSelector;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelState : MonoBehaviour

{
    public Sprite lowScore;
    public Sprite mediumScore;
    public Sprite highScore;

    private int levelIndex;
    private Level currLevel = new Level();
    private int maxPoints;

    public void Start()
    {
        string currName = PlayerPrefs.GetString(this.gameObject.name + "-Data");
        this.currLevel = JsonUtility.FromJson<Level>(currName);

        this.maxPoints = this.currLevel.levelMaxScore;


        //TODO: Fix
        LevelProgressStatic.levelProgress[this.gameObject.name] = 0;
        if (LevelProgressStatic.levelProgress[this.gameObject.name] != 0)
        {

            this.levelIndex = LevelProgressStatic.StarCounter(LevelProgressStatic.levelProgress[this.gameObject.name], this.maxPoints);

            Debug.Log(LevelProgressStatic.StarCounter(LevelProgressStatic.levelProgress[this.gameObject.name], this.maxPoints));


            this.SwitchState(this.levelIndex);
        }
    }


    public void SwitchState(int points)
    {

        Sprite currSprite = null;

        if (points == 1)
        {
            currSprite = this.lowScore;
        }
        else if (points == 2)
        {
            currSprite = this.mediumScore;
        }
        else if (points == 3)
        {
            currSprite = this.highScore;
        }

        this.gameObject.GetComponent<Image>().overrideSprite = currSprite;


    }
}
