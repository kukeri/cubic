﻿using UnityEngine;
using Assets.Scripts.Cuby;
using Assets.Scripts.Json;
using Assets.Scripts.UI.LevelSelector;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class ChangeLavelState : MonoBehaviour
{
    public Sprite lowScore;
    public Sprite mediumScore;
    public Sprite highScore;
    private int maxPoints;
    private Level currLevel = new Level();
    private bool isSet;
    private int spriteIndex;

    void Update()
    {
        if (!CubyStaticData.EndOfGame && !this.isSet)
        {

            string currName = PlayerPrefs.GetString(SceneManager.GetActiveScene().name + "-Data");
            this.currLevel = JsonUtility.FromJson<Level>(currName);

            this.maxPoints = this.currLevel.levelMaxScore;
            string currentLevelName = SceneManager.GetActiveScene().name;
            this.maxPoints = this.currLevel.levelMaxScore;
            int starPoints = LevelProgressStatic.StarCounter(CubyStaticData.CurrScore, this.maxPoints);
            int currntPoints = LevelProgressStatic.StarCounter(LevelProgressStatic.levelProgress[currentLevelName], this.maxPoints);

            this.SwitchState(starPoints);
            this.isSet = true;

            if (starPoints >= currntPoints)
            {
                LevelProgressStatic.levelProgress[SceneManager.GetActiveScene().name] = CubyStaticData.CurrScore;

                this.StartCoroutine(LevelProgressStatic.SaveScoreToServer(Social.localUser.userName, CubyStaticData.CurrScore, currentLevelName));
            }

            CubyStaticData.CurrScore = 0;
        }
    }

    public void SwitchState(int points)
    {

        Sprite currSprite = null;

        if (points == 1)
        {
            currSprite = this.lowScore;
        }
        else if (points == 2)
        {
            currSprite = this.mediumScore;
        }
        else if (points == 3)
        {
            currSprite = this.highScore;
        }

        this.gameObject.GetComponent<Image>().overrideSprite = currSprite;

    }
}
