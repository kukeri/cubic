﻿namespace Assets.Scripts.UI.LevelSelector
{
    using System.Collections;
    using System.Collections.Generic;
    using PHP;
    using UnityEngine;

    public static class LevelProgressStatic
    {
        public static Dictionary<string, int> levelProgress = new Dictionary<string, int>();


        public static int StarCounter(int points, int maxPoints)
        {
            int starCounter = 0;

            if (points > maxPoints * 2 / 5 && points < maxPoints * 3 / 4)
            {
                starCounter = 1;
            }
            else if (points > maxPoints * 3 / 4 && points < maxPoints)
            {
                starCounter = 2;
            }
            else if (points >= maxPoints)
            {
                starCounter = 3;
            }

            return starCounter;
        }



        public static IEnumerator SaveScoreToServer(string userName, int score, string levelName)
        {
            WWWForm scoreForm = new WWWForm();
            scoreForm.AddField("Score", score);
            scoreForm.AddField("NickName", userName);
            scoreForm.AddField("LevelName", levelName);
            WWW sendScore = new WWW(Servers.addScore, scoreForm);

            yield return sendScore;

            Debug.Log(sendScore.text);
        }
    }
}
