﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.UI.InGame;
using UnityEngine.UI;

public class PausedGameButton : MonoBehaviour
{
    public Image play;
    public Image pause;

    private Image currImage;

    void Start()
    {
        this.currImage = this.GetComponent<Image>();
    }


    void Update()
    {
        this.currImage.sprite = !LevelStaticData.gameIsPaused ? this.pause.sprite : this.play.sprite;
    }
}
