﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BulletChanger : MonoBehaviour
{
    public CubyFire cuby;

    public void ChangeToGivenWeapon(GameObject weapon)
    {
        cuby.specialWeapon = weapon;
        ClosePanel();
    }

    private void ClosePanel()
    {
        this.gameObject.SetActive(false);
    }
}
