﻿using Assets.Scripts.Cuby;
using Assets.Scripts.Json;
using Assets.Scripts.Meteorites;
using Assets.Scripts.UI.InGame;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public HintsGiverScript hinterPanel;

    public GameObject xOfStreak;
    public GameObject threeOfStreak;
    public GameObject fiveOfStreak;

    public GameObject joystick;
    public GameObject leftButton;
    public GameObject rightButton;

    public GameObject replayButton;
    public GameObject pauseButton;
    public GameObject mainMenuButton;

    public GameObject gameOverImg;
    public Image puseImage;
    public GameObject bulletPicker;
    public Text coinsCounter;
    public Text coinsWon;
    public Text finalScore;
    public Text RegularBulletCounter;
    private Player playerData = new Player();
    private Level level;

    private bool bulletOpenerPending = false;
    private float secondsTillBulletPickerOpen = 1.0f;

    private string player;

    void Awake()
    {
        SaveScore(2);
    }

    void Start()
    {
        player = PlayerPrefs.GetString("Cuby");

        CubyStaticData.Score = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name);

        string currentLevelData = PlayerPrefs.GetString(SceneManager.GetActiveScene().name + "-Data");
        this.ExtractData(currentLevelData);

        SetPredefinedSettings();
        SetActiveController();

    }

    private void ExtractData(string currLevel)
    {
        this.level = JsonUtility.FromJson<Level>(currLevel);
    }

    private void SetPredefinedSettings()
    {
        CubyStaticData.IsAlive = true;
        Screen.orientation = ScreenOrientation.Portrait;
        this.gameOverImg.SetActive(false);
        this.replayButton.SetActive(false);
        this.xOfStreak.SetActive(false);
        this.threeOfStreak.SetActive(false);
        this.fiveOfStreak.SetActive(false);
        this.mainMenuButton.SetActive(false);
        hinterPanel.gameObject.SetActive(true);
        hinterPanel.SetWantedAdviceSet(SceneManager.GetActiveScene().name);
    }

    private void SetActiveController()
    {
        string activeController = PlayerPrefs.GetString("PlayMode", "Joystick");

        if (activeController == "Tilt")
        {
            this.joystick.SetActive(false);
            this.leftButton.SetActive(false);
            this.rightButton.SetActive(false);
        }
        else if (activeController == "Joystick")
        {
            this.joystick.SetActive(true);
            this.leftButton.SetActive(false);
            this.rightButton.SetActive(false);
        }
        else if (activeController == "Buttons")
        {
            this.leftButton.SetActive(true);
            this.rightButton.SetActive(true);
            this.joystick.SetActive(false);
        }
    }

    void Update()
    {
        if (CubyStaticData.EndOfGame)
        {
            this.CheckForSavingScore();

            this.EndGame();

        }
        else
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                this.PauseGame();
            }
            this.RegularBulletCounter.text = CubyStaticData.NumberOfRegularBullets.ToString();
        }


        if (!CubyStaticData.IsAlive)
        {
            this.GameOverState();
        }

        if (CubyMovement.currentStreak == 3)
        {
            this.ActivateBonusX3();
        }
        else if (CubyMovement.currentStreak == 5)
        {
            this.ActivateBonusX5();
        }
        else
        {
            this.ResetBonus();
        }

        this.CheckForStreaks();
        this.coinsCounter.text = CubyStaticData.Coins.ToString();


    }

    private void EndGame()
    {
        this.coinsWon.text = CubyStaticData.Coins.ToString();
        this.finalScore.text = CubyStaticData.CurrScore.ToString();
        string level1 = JsonUtility.ToJson(this.level);
        PlayerPrefs.SetString(SceneManager.GetActiveScene().name + "-Data", level1);
        CubyStaticData.EndOfGame = false;

        CubyStaticData.Coins += PlayerPrefs.GetInt("Coins");

        PlayerPrefs.SetInt("Coins", CubyStaticData.Coins);
    }

    void OnGUI()
    {
        this.CheckForBulletPickerOpenner();

    }

    public void ResetBonus()
    {
        this.xOfStreak.SetActive(false);
        this.threeOfStreak.SetActive(false);
        this.fiveOfStreak.SetActive(false);
    }

    public void StartPendingForBulletChoserToOpen()
    {
        this.bulletOpenerPending = true;
    }

    public void NextLevel()
    {
        int levelCount = SceneManager.GetActiveScene().buildIndex;
        CubyStaticData.CurrScore = 0;
        MeteoriteStaticData.ResetMeteoritesDestroyed();
        SceneManager.LoadScene(levelCount + 1);

    }

    public void StopPendingForBulletChoserToOpen()
    {
        this.bulletOpenerPending = false;
        this.secondsTillBulletPickerOpen = 1.0f;
    }

    public void ActivateBonusX3()
    {
        this.xOfStreak.SetActive(true);
        this.threeOfStreak.SetActive(true);
    }

    public void ActivateBonusX5()
    {
        this.xOfStreak.SetActive(true);
        this.fiveOfStreak.SetActive(true);
    }

    public void BulletChosen()
    {
        this.bulletPicker.SetActive(false);
        this.pauseButton.SetActive(true);
        this.PauseGame();
    }

    public void ReturnToMainMenu()
    {
        this.Reset();
        MeteoriteStaticData.ResetMeteoritesDestroyed();
        LevelStaticData.gameIsPaused = true;
        this.PauseGame();
        SceneManager.LoadScene(ScenesEnum.LevelSelector.ToString());
    }

    public void ReturnToMainMenuWinPanel()
    {
        this.CheckForSavingScore();

        this.Reset();

        MeteoriteStaticData.ResetMeteoritesDestroyed();

        SceneManager.LoadScene(ScenesEnum.LevelSelector.ToString());
    }

    private void CheckForSavingScore()
    {

        if (CubyStaticData.Score <= CubyStaticData.CurrScore)
        {
            this.SaveScore(CubyStaticData.CurrScore - CubyStaticData.Score);
        }

    }

    public void RestartCurrentScene()
    {
        CubyStaticData.CurrScore = 0;
        CubyStaticData.IsAlive = true;
        LevelStaticData.gameIsPaused = true;
        this.PauseGame();
        this.ResetCubySpecifics();
        int currentLevel = SceneManager.GetActiveScene().buildIndex;
        MeteoriteStaticData.ResetMeteoritesDestroyed();
        SceneManager.LoadScene(currentLevel);
    }

    public void PauseGame()
    {
        if (LevelStaticData.gameIsPaused)
        {
            UnpauseGame();
        }
        else
        {
            LevelStaticData.gameIsPaused = true;
            this.replayButton.SetActive(true);
            this.mainMenuButton.SetActive(true);
            Time.timeScale = 0;
            MusicScirpt.PauseMusic();
        }
    }

    public void UnpauseGame()
    {
        this.replayButton.SetActive(false);
        this.mainMenuButton.SetActive(false);
        LevelStaticData.gameIsPaused = false;
        Time.timeScale = 1;
        MusicScirpt.UnpauseMusic();
    }

    private void CheckForStreaks()
    {
        if (CubyMovement.currentStreak == 3)
        {
            this.ActivateBonusX3();
        }
        else if (CubyMovement.currentStreak == 5)
        {
            this.ActivateBonusX5();
        }
        else
        {
            this.ResetBonus();
        }
    }

    private void CheckForBulletPickerOpenner()
    {
        if (this.bulletOpenerPending)
        {
            this.secondsTillBulletPickerOpen -= Time.deltaTime;

            if (this.secondsTillBulletPickerOpen <= 0)
            {
                this.PauseGame();
                this.bulletPicker.SetActive(true);
                this.bulletOpenerPending = false;
                this.secondsTillBulletPickerOpen = 1.0f;
            }
        }
    }

    void GameOverState()
    {
        if (!CubyStaticData.IsAlive)
        {
            this.gameOverImg.SetActive(true);
            this.replayButton.SetActive(true);
            this.mainMenuButton.SetActive(true);
        }
    }

    private void Reset()
    {

        this.gameOverImg.SetActive(false);
        this.replayButton.SetActive(false);
        this.mainMenuButton.SetActive(false);
        CubyStaticData.IsAlive = true;
        CubyStaticData.CurrScore = 0;
        this.ResetCubySpecifics();
        CubyStaticData.NumberOfRegularBullets = this.playerData.regularBullets;

    }

    private void ResetCubySpecifics()
    {
        string player = PlayerPrefs.GetString("Cuby");
        this.playerData = JsonUtility.FromJson<Player>(player);
        this.playerData.regularBullets = this.level.numberOfRegularBullets;
        this.playerData.Score = 0;
        CubyStaticData.CurrScore = 0;
        CubyStaticData.NumberOfRegularBullets = 0;
    }

    public void SaveScore(int currScore)
    {

#if UNITY_ANDROID
        this.playerData = JsonUtility.FromJson<Player>(player);
#endif

#if UNITY_EDITOR 
        this.playerData = new Player();
        this.playerData.Name = "Kur";
        this.playerData.Score = 2;
        this.playerData.regularBullets = 20;
#endif

        string currentLevelName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetInt(currentLevelName, currScore);

        this.playerData.Score += currScore;
        string playerData = JsonUtility.ToJson(this.playerData);
        PlayerPrefs.SetString("Cuby", playerData);

    }
}
