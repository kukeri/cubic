﻿using UnityEngine;
using System.Collections;

public class StreakScript : MonoBehaviour
{
    private SpriteRenderer sprite;

    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (CubyMovement.currentStreak == 3)
        {
            sprite.sortingOrder = 1;
            var trasnsform = sprite.transform.localScale;
            trasnsform.x += 0.07f;
            trasnsform.y += 0.07f;
            sprite.transform.localScale = trasnsform;
        }
        else
        {
            sprite.sortingOrder = -1;
            var scale = sprite.transform.localScale;
            scale.x = 1;
            scale.y = 1;
            sprite.transform.localScale = scale;
        }
    }

}
