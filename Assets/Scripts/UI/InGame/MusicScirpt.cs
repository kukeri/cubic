﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using Assets.Scripts.UI.Settings;

public class MusicScirpt : MonoBehaviour
{
    private static AudioSource backgroundMusic;

    void Start()
    {
        backgroundMusic = this.gameObject.GetComponent<AudioSource>();
        if (!CurrentSettings.isMusicMuted)
        {
            backgroundMusic.Play();
        }
    }

    public static void PauseMusic()
    {
        backgroundMusic.Pause();
    }

    public static void UnpauseMusic()
    {
        backgroundMusic.UnPause();
    }

}

