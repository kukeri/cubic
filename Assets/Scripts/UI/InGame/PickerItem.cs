﻿using UnityEngine;
using System.Collections;

public class PickerItem : MonoBehaviour
{

    public GameObject correspondingComponent;

    public void PickItem()
    {
        this.gameObject.GetComponentInParent<BulletChanger>().ChangeToGivenWeapon(correspondingComponent);
    }
}
