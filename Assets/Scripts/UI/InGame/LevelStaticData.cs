﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Assets.Standard_Assets.CrossPlatformInput.Scripts.PlatformSpecific;

namespace Assets.Scripts.UI.InGame
{
    public static class LevelStaticData
    {
        public static bool LevelHasEnded;
        public static bool gameIsPaused;

        public static void ResetLevelState()
        {
            LevelHasEnded = false;
        }
    }
}
