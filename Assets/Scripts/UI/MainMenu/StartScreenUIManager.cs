﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.SceneManagement;

public class StartScreenUIManager : MonoBehaviour
{

    public GameObject loadingScreen;

    public void Awake()
    {
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            this.QuitApplication();
        }
    }

    private IEnumerator LoadLevelAsync(ScenesEnum scene)
    {
        this.loadingScreen.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync(scene.ToString());

        yield return async;
    }

    public void StartGame()
    {
        if (PlayerPrefs.GetInt("isFirstStart", 1) == 1)
        {
            PlayerPrefs.SetInt("isFirstStart", 0);
            StartCoroutine(this.LoadLevelAsync(ScenesEnum.TutorialLevel));
        }
        else
        {
            StartCoroutine(this.LoadLevelAsync(ScenesEnum.LevelSelector));
        }
    }

    public void OpenSettings()
    {
        SceneManager.LoadScene(ScenesEnum.SettingsScene.ToString());
    }

    public void OpenAchievements()
    {
        SceneManager.LoadScene(ScenesEnum.Achievements.ToString());
    }

    public void OpenStore()
    {
        SceneManager.LoadScene(ScenesEnum.Store.ToString());
    }

    public void StartSurvival()
    {
        Debug.Log("Not implemented!");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

}
