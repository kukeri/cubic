﻿using UnityEngine;
using System.Collections;

public enum ScenesEnum
{
    StartScene, SettingsScene, Achievements, LevelSelector, Store, Level1, Level2, Level3, Level4, Level5, TutorialLevel
}
