﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HintsGiverScript : MonoBehaviour
{
    private Action currentAction;

    public Text textBoxOfHinter;
    public Image[] usedImages;

    private int currentMsg;

    // Use this for initialization
    void Start()
    {
    }

    //Seting the advice set by name 
    public void SetWantedAdviceSet(string adviceSetName)
    {
        this.currentMsg = -1;
        Time.timeScale = 0;
        if (adviceSetName == ScenesEnum.TutorialLevel.ToString())
        {
            currentAction = TutorialSet;
        }
        if (adviceSetName == ScenesEnum.Level1.ToString())
        {
            currentAction = Level1;
        }

        currentAction();
    }

    //When next is clicked
    public void GetNextAdvice()
    {
        currentAction();
    }

    //When skip is clicked
    public void SkipCurrentAdvice()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
        this.currentMsg = -1;
    }

    //All the message sets
    #region  
    private void TutorialSet()
    {
        this.currentMsg++;
        print(this.currentMsg);
        if (currentMsg == 0)
        {
            this.textBoxOfHinter.text = "Hello there, noobie. You think you have what it takes to save the universe?";
        }
        else
        {
            this.currentMsg = -1;
            this.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    private void Level1()
    {
        this.currentMsg++;
        print(this.currentMsg);
        if (currentMsg == 0)
        {
            this.textBoxOfHinter.text = "Hello there, noobie. You think you have what it takes to save the universe?";
        }
        else
        {
            this.currentMsg = -1;
            this.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
    #endregion
}
