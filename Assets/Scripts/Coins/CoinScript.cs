﻿using UnityEngine;

public class CoinScript : MonoBehaviour
{       
    private GameObject cuby;
           
    void Start()
    {
        this.cuby = GameObject.Find("Cuby");
    }

    void Update()
    {

        float coinCubyDistance = (this.cuby.transform.position.x - this.transform.position.x) / 20;
        this.transform.Translate(new Vector3(coinCubyDistance, 0, 0));
    }

}
