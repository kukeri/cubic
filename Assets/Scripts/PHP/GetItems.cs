﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Json;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GetItems : MonoBehaviour
{
    private string getItems = "http://cubick.cloudvps.bg/Cuby/GetItems.php";
    private Text currText;
    private string text;
    private Dictionary<string, int> storeData = new Dictionary<string, int>();
    public Text[] names;
    public Text[] prices;
    private int currentCoints;
    public GameObject noMoney;


    IEnumerator Start()
    {
        WWW items = new WWW(this.getItems);

        yield return items;
        this.currentCoints = PlayerPrefs.GetInt("Coins");
        this.currText = this.GetComponent<Text>();
        this.ExtractData(items.text);
        this.WriteData();
    }

    public void ExitNoMoney()
    {
        this.noMoney.SetActive(false);
    }

    public void BuyItem(Text itemName)
    {
        int price = this.storeData[itemName.text];

        if (this.currentCoints <= price)
        {
            this.noMoney.SetActive(true);
            var noMoney = this.noMoney.GetComponentInChildren<Text>();
            noMoney.text = noMoney.text + " " + (price - this.currentCoints) + " " + "Needed";
        }
        else
        {
            this.currentCoints -= price;
            PlayerPrefs.SetInt("Coins", this.currentCoints);
        }

    }

    private void ExtractData(string inputData)
    {

        List<string> inputArray = inputData.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        inputArray.RemoveAt(inputArray.Count - 1);

        foreach (var data in inputArray)
        {
            string[] tempArray = data.Split('|');
            string name = tempArray[0].Trim();
            int price = int.Parse(tempArray[1]);
            this.storeData.Add(name, price);
        }
    }

    private void WriteData()
    {
        for (int i = 0; i < this.names.Length; i++)
        {
            string itemName = this.names[i].text;

            this.prices[i].text = String.Format(this.storeData[itemName].ToString());
        }
    }

}
