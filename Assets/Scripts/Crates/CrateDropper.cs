﻿using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Json;
using Assets.Scripts.UI.InGame;
using UnityEngine.SceneManagement;

public class CrateDropper : MonoBehaviour
{
    public GameObject Crate;
    private float width;
    private float crateTimer;
    private Level level;

    void Start()
    {   
        string currLevel = PlayerPrefs.GetString(SceneManager.GetActiveScene().name + "-Data");

        this.ExtractData(currLevel);
    }

    private void ExtractData(string currLevel)
    {
        this.level = JsonUtility.FromJson<Level>(currLevel);
        this.crateTimer = this.level.normalCrateTimer;     
    }

    void Update()
    {
        if (!LevelStaticData.LevelHasEnded)
        {
            this.TryDropPossibleMeteorites();
        }
    }

    private void TryDropPossibleMeteorites()
    {
        this.CrateCheckDrop(this.Crate, ref this.crateTimer);
    }

    private void CrateCheckDrop(GameObject crate, ref float timer)
    {                             
        timer -= Time.deltaTime;    

        if (timer <= 0)
        {
            this.DropCrate(crate);
            timer = this.level.normalCrateTimer;
        }         
    }             

    private void DropCrate(GameObject create)
    {
        this.width = this.GetScreenSize();

        Instantiate(create, new Vector2(Random.Range(-this.width + 1, this.width - 1), 8f),
            Quaternion.Euler(Vector3.zero));
    }

    float GetScreenSize()
    {
        float worldScreenHeight = Camera.main.orthographicSize;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        return worldScreenWidth;
    }
}
