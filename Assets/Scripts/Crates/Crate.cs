﻿using UnityEngine;
using System.Collections;

public class Crate : MonoBehaviour
{
    public GameObject coin;
    public GameObject regularBulletsBonus;

    protected Animator animator;
    protected new Rigidbody2D rigidbody;
    protected BoxCollider2D isCollidable;

    public float verticalForce;
    public float horizontalForce;

    protected double health;
                                    

    public virtual void Start()
    {                                  
        GetWantedComponents();
        InitialiseObjectData();
    }

    protected virtual void InitialiseObjectData()
    {
        this.health = 2;
        this.verticalForce = 2.5f;
        this.horizontalForce = 0f;
    }

    protected virtual void GetWantedComponents()
    {
        this.rigidbody = this.GetComponent<Rigidbody2D>();
        this.animator = this.GetComponent<Animator>();
        this.isCollidable = this.GetComponentInChildren<BoxCollider2D>();
    }

    protected virtual void Update()
    {
        this.rigidbody.velocity = new Vector2(horizontalForce, -verticalForce);
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        this.CheckForBulletColiision(other);
    }

    public void DestroyCrate()
    {
        Destroy(this.gameObject);
    }

    public virtual void OnCollisionEnter2D(Collision2D other)
    {
        this.CheckForFloorCollision(other);
        this.CheckForPlayerCollision(other);
        this.CheckForForceFieldCollision(other);
    }

    protected virtual void CheckForPlayerCollision(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            CubyMovement.currentStreak = 0;
            Destroy(this.gameObject);
        }
    }

    protected virtual void CheckForFloorCollision(Collision2D other)
    {
        if (other.gameObject.tag == ("MeteoritFloor") || other.gameObject.tag == "Floor")
        {
            this.animator.SetTrigger("Floor");
        }
    }

    protected virtual void CheckForForceFieldCollision(Collision2D other)
    {
        if (other.gameObject.tag == "ForceField")
        {
            DestroyCrate();
        }
    }

    protected virtual void CheckForBulletColiision(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            if (this.health == 2)
            {
                this.animator.SetTrigger("Crate");
            }
            else if (this.health == 1)
            {
                int chance = Random.Range(0, 1000);
                this.animator.SetTrigger("Break");       
                this.isCollidable.enabled = false;

                print(chance);
                if (chance < 100f)
                {                                                                                  
                    Instantiate(regularBulletsBonus, this.transform.position, Quaternion.identity);
                }
                else
                {
                    for (int i = 0; i < 5; i++)
                    {
                        Instantiate(coin, this.transform.position, Quaternion.identity);
                    }
                }
            }
            else
            {
                this.health = 3;
            }
            health--;
        }
    }
}
