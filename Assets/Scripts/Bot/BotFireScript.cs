﻿using UnityEngine;
using System.Collections;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Bot;
using Assets.Scripts.Cuby;
using Assets.Scripts.UI.InGame;
using Assets.Scripts.UI.Settings;

public class BotFireScript : MonoBehaviour
{
    private const float FireDelay = 0.2f;
    private const float ChanceToFireForNoReason = 200;
    private const float ChanceToFireWhenUnderMeteorite = 400;

    public Bullet normalBullet;
    public AudioSource attachedFireSound;

    private float fireTime;
    private float offsetForPositionOfBulletFire;
    private Animator attachedAnimator;
    private Rigidbody2D attachedRigidbody;


    void Start()
    {
        Assets.Scripts.Bot.BotStaticData.IsAlive = true;
        this.fireTime = 0;
        this.attachedAnimator = this.gameObject.GetComponent<Animator>();
        this.attachedRigidbody = this.gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (BotStaticData.currentMeteoriteOnFocus == null)
        {
            TryToSetCurrentMeteoriteOnFocus();
         
        }

        fireTime += Time.deltaTime;
        print(fireTime);
        bool isAlignedWithWantedMeteorite = false;
        if (BotStaticData.currentMeteoriteOnFocus != null)
        {
            isAlignedWithWantedMeteorite = BotStaticData.currentMeteoriteOnFocus.transform.position.y !=
                                                this.gameObject.transform.position.y;
        }

        if (fireTime >= FireDelay && !isAlignedWithWantedMeteorite)
        {
            print("Unialigned");
            float chance = Random.Range(0, 1000);
            if (chance < ChanceToFireForNoReason)
            {
                Fire();
            }
            this.fireTime = 0f;

        }
        else if (fireTime >= FireDelay && isAlignedWithWantedMeteorite)
        {
            print("Aligned");
            float chance = Random.Range(0, 1000);
            if (chance < ChanceToFireWhenUnderMeteorite)
            {
                Fire();
            }

            this.fireTime = 0f;
        };
    }

    public void TryToSetCurrentMeteoriteOnFocus()
    {
        float minimalDistance = float.MaxValue;
        MeteoriteStateChanger[] meteorites = FindObjectsOfType<MeteoriteStateChanger>();
        if (meteorites != null)
        {
            foreach (var meteorite in meteorites)
            {
                float distance = FindDistanceToMeteorite(meteorite.transform);
                if (distance < minimalDistance)
                {
                    minimalDistance = distance;
                    Assets.Scripts.Bot.BotStaticData.currentMeteoriteOnFocus = meteorite;
                }
            }
        }
    }

    public float FindDistanceToMeteorite(Transform meteorite)
    {
        return Vector2.Distance(meteorite.position, this.gameObject.transform.position);
    }

    public void Fire()
    {
        if (Assets.Scripts.Cuby.CubyStaticData.IsAlive && !LevelStaticData.gameIsPaused && Assets.Scripts.Bot.BotStaticData.IsAlive)
        {
            if (Assets.Scripts.Cuby.CubyStaticData.NumberOfRegularBullets > 0)
            {
                this.PlayFireAnimation();
                Instantiate(this.normalBullet, new Vector2(this.attachedRigidbody.transform.position.x + this.offsetForPositionOfBulletFire, this.attachedRigidbody.transform.position.y + 1f), Quaternion.identity);
                Assets.Scripts.Cuby.CubyStaticData.NumberOfRegularBullets--;

                if (!CurrentSettings.isFXMuted)
                {
                    this.attachedFireSound.Play();
                }
            }

            Assets.Scripts.Bot.BotStaticData.Speed = Assets.Scripts.Bot.BotStaticData.BotIdle;
            this.fireTime = 0f;
        }
    }

    private void PlayFireAnimation()
    {
        if (Assets.Scripts.Bot.BotStaticData.Speed < 0)
        {
            attachedAnimator.SetTrigger("FireLeft");
            this.offsetForPositionOfBulletFire = -0.3f;
        }
        else
        {
            attachedAnimator.SetTrigger("FireRight");
            this.offsetForPositionOfBulletFire = 0.3f;
        }

    }
}
