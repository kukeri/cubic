﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Bot
{
    public static class BotStaticData
    {
        public static MeteoriteStateChanger currentMeteoriteOnFocus;

        public static readonly int BotIdle = 0;
        public static readonly int BotNormalSpeed = 6;

        public static int NumberOfRegularBullets;     

        public static bool IsAlive;
        public static int Speed;
        public static int[] meteoritesDestroyed; // Use indexation with the enumeration meteorites 

    }
}
