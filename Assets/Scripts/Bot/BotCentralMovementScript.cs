﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Bot;

public class BotCentralMovementScript : MonoBehaviour
{

    public AudioSource WalkingAudioSource;
    private Animator attachedAnimator;
    private Rigidbody2D attachedRigidbody;
                              
    void Start()
    {
        BotStaticData.Speed = BotStaticData.BotNormalSpeed;
        this.attachedRigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        this.attachedAnimator = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (BotStaticData.currentMeteoriteOnFocus != null)
        {
            float distanceInXDirection = this.transform.position.x - BotStaticData.currentMeteoriteOnFocus.transform.position.x;

            if (distanceInXDirection > 0.2f)
            {    MoveLeft();                                                                     
            }
            else if (distanceInXDirection < -0.6f)
            {               
                MoveRight();                                                              
            }
            else
            {
                SetToIdle();
            }

            this.attachedRigidbody.velocity = new Vector2(BotStaticData.Speed, 0);



        }      
    }

    public void SetToIdle()
    {
        if (BotStaticData.IsAlive)
        {
            WalkingAudioSource.Stop();
            BotStaticData.Speed = BotStaticData.BotIdle;
            this.attachedAnimator.SetTrigger("IdleState");
        }
    }

    private void MoveLeft()
    {
        if (BotStaticData.IsAlive)
        {
            if (!WalkingAudioSource.isPlaying)
            {
                WalkingAudioSource.Play();
            }

            BotStaticData.Speed = -BotStaticData.BotNormalSpeed;
            attachedAnimator.SetTrigger("WalkingLeft");
        }
    }

    private void MoveRight()
    {
        if (BotStaticData.IsAlive)
        {
            if (!WalkingAudioSource.isPlaying)
            {
                WalkingAudioSource.Play();
            }
            BotStaticData.Speed = BotStaticData.BotNormalSpeed;
            attachedAnimator.SetTrigger("WalkingRight");
        }
    }
}
