﻿using UnityEngine;

namespace Assets.Scripts.Bullets
{                         
    public class HomingBullet : Bullet
    {
        private GameObject currMeteor;

        public GameObject currY;

        protected void Start()
        {
            if (GameObject.FindGameObjectWithTag("Meteorit"))
            {
                this.FindMeteorit();
            }
        }

        protected override void Update()
        {
            if (GameObject.FindGameObjectWithTag("Meteorit") && this.currMeteor != null)
            {
                MoveTowardsMeteorite();
                RotateTowardsMeteorite();
            }
            else
            {
                FindMeteorit();
                this.transform.Translate(0, 0.2f, 0);
            }
        }

        private void MoveTowardsMeteorite()
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position,
       this.currMeteor.transform.position,
       10 * Time.deltaTime);
        }

        private void RotateTowardsMeteorite()
        {
            Vector3 targetDir = this.currMeteor.transform.position - transform.position;
            float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, -angle));
        }

        void FixedUpdate()
        {
            if (this.transform.position.y >
                Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).y)
            {
                Destroy(this.gameObject);
            }
        }

        private void FindMeteorit()
        {
            GameObject[] meteors = GameObject.FindGameObjectsWithTag("Meteorit");

            for (int i = 0; i < meteors.Length; i++)
            {
                if (Vector2.Distance(meteors[i].transform.position, this.currY.transform.position) < 19 &&
                    meteors[i].activeInHierarchy)
                {
                    this.currMeteor = meteors[i];
                    break;
                }
            }
        }
    }
}

