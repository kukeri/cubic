﻿using UnityEngine;

public class ShrapnelBullet : Bullet
{
    public Bullet Bullet;   
    private float timer = 0.3f;  

    protected override void Update()
    {
        this.timer -= Time.deltaTime;

        if (this.timer <= 0)
        {

            for (int i = 0; i < 100; i += 10)
            {
                var pieceRotation = Quaternion.AngleAxis(320 + i, Vector3.forward);

                Instantiate(this.Bullet, this.transform.position, pieceRotation);

            }
            Destroy(this.gameObject);

        }

        this.transform.Translate(new Vector3(0, Time.deltaTime * 10, 0));
    }
}
