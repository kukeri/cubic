﻿using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    protected  void OnTriggerEnter2D(Collider2D other)
    {
        if (CubyMovement.currentStreak == 3)
        {
            print("x3");
        }
        if (CubyMovement.currentStreak == 5)
        {
            print("x5");
        }

        Destroy(this.gameObject);

    }

    protected abstract void Update();

    void FixedUpdate()
    {
        if (this.transform.position.y > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).y
           || this.transform.position.x > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).x
           || this.transform.position.x < Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).x * -1)
        {
            Destroy(this.gameObject);
        }
    }
}
