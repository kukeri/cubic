﻿using UnityEngine;
using System.Collections;

public class HomingRainBullet : Bullet
{

    public GameObject hommingBullet;
    public GameObject regularBullet;

    protected override void Update()
    {
        this.Moving();
    }

    private void Moving()
    {
        if (this.transform.position.y < Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).y - 1)
        {
            this.transform.Translate(0, 0.3f, 0);
        }
        else
        {
            //Make new bullet and Instantiate form it bunch of homingBullets
            Destroy(this.gameObject);

            var left = Quaternion.AngleAxis(90, Vector3.forward);
            var right = Quaternion.AngleAxis(270, Vector3.forward);

            Instantiate(this.regularBullet, this.transform.position, left);
            Instantiate(this.regularBullet, this.transform.position, right);

            for (int i = 0; i < 10; i++)
            {
                Instantiate(this.hommingBullet, this.transform.position, right);
            }

        }
    }
}
