﻿using UnityEngine;
using Assets.Scripts.Cuby;
using Assets.Scripts.Json;
using UnityEngine.SceneManagement;

public class BulletMovingScript : MonoBehaviour
{

    Level currLevel = new Level();


    void OnTriggerEnter2D(Collider2D other)
    {
        if (CubyMovement.currentStreak == 3)
        {
            print("x3");
        }
        if (CubyMovement.currentStreak == 5)
        {
            print("x5");
        }

        Destroy(this.gameObject);
    }

    void Update()
    {
        var position = this.transform.position;
        position.y += 0.3f;
        this.transform.position = position;
    }

    void FixedUpdate()
    {
        if (this.transform.position.y > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).y)
        {
            Destroy(this.gameObject);
        }
    }
}
