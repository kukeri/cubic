﻿namespace Assets.Scripts.Bullets
{
    using UnityEngine;

    class RegularBullet : Bullet
    {  
        protected override void Update()
        {
            this.transform.Translate(Vector2.up * Time.deltaTime * 10);
        }
    }
}
