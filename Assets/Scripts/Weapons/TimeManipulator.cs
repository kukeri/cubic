﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Meteorites;

public class TimeManipulator : MonoBehaviour
{
    private bool isSlowed = false;
    private float slowedTimeScale = 0.4f;
    private float normalTimeScale = 1f;

    public void ChangeMeteoriteTimeScale()
    {
        if (isSlowed)
        {
            MeteoriteStaticData.timeScale = normalTimeScale;
            isSlowed = false;
        }
        else
        {
            MeteoriteStaticData.timeScale = slowedTimeScale;
            isSlowed = true;
        }
    }
}
