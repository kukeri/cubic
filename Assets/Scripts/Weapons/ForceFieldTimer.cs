﻿using UnityEngine;
using System.Collections;

public class ForceFieldTimer : MonoBehaviour {

	// Use this for initialization
    private bool hasStartedCountdown;
    private float numberOfSecondsForForceField = 2.0f;
	
	// Update is called once per frame
	void Update () {
	    if (this.isActiveAndEnabled && !hasStartedCountdown)
	    {
	        hasStartedCountdown = true;
	    }

	    if (hasStartedCountdown)
	    {
	        numberOfSecondsForForceField -= Time.deltaTime;
	        if (numberOfSecondsForForceField <= 0)
	        {
	            numberOfSecondsForForceField = 2.0f;
                this.hasStartedCountdown = false;
                this.gameObject.SetActive(false);
	        }
	    }
	   
	}
}
