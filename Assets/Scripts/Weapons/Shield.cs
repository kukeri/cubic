﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Cuby;

public class Shield : MonoBehaviour
{

    // Use this for initialization
    private int health;
    void Start()
    {
        CubyStaticData.Health = 3;
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Meteorit")
        {                                 
            CubyStaticData.Health--;
            Destroy(other.gameObject);

            if (CubyStaticData.Health == 0)
            {
                CubyStaticData.Health = 0;
                Destroy(this.gameObject);
            }
        }
    }
}
