﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;

public class HurricaneMine : MonoBehaviour
{

    public int health;

    private MeteoriteStateChanger[] meteorites;
    private bool HasOpenned = false;
    private float deltaTimeForPulling = 0f;

    void Start()
    {
        this.health = 5;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Meteorit")
        {
            health--;

            Destroy(other.gameObject);
            if (health <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    void Update()
    {
        deltaTimeForPulling -= Time.deltaTime;
        if (deltaTimeForPulling <= 0)
        {
            if (HasOpenned)
            {
                deltaTimeForPulling = 1f;
                meteorites = FindObjectsOfType<MeteoriteStateChanger>();
            }
        }

        if (meteorites != null)
        {
            foreach (var meteoriteTransform in meteorites)
            {
                if (meteoriteTransform != null)
                {      
                    float hurricaneMeteoriteDistance = (meteoriteTransform.gameObject.transform.position.x - this.transform.position.x) / 20;
                    print(hurricaneMeteoriteDistance);
                    if (Mathf.Abs(hurricaneMeteoriteDistance) < 0.15)
                    {
                        meteoriteTransform.gameObject.transform.Translate(-new Vector3(hurricaneMeteoriteDistance, 0, 0));
                    }   
                }
            }
        }     
    }

    public void Open()
    {
        this.HasOpenned = true;
    }
}
