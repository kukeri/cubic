﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class LaserMine : MonoBehaviour {

    public int health;
                 
    void Start()
    {
        this.health = 2;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
      
        if (other.gameObject.tag == "SpinningMeteorite")
        {
            print("hello");
            health--;   
            Destroy(other.gameObject);
            if (health <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }
    
        
}
