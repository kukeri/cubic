﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class MagnetMine : MonoBehaviour
{
    public int health;
    private List<HardMeteoritStateChanger> meteorites;
    private float deltaTimeForPulling = 0f;
    private Animator attachedAnimator;      
    private bool canPull;

    private HardMeteoritStateChanger currentMeteorite;

    void Start()
    {
        this.health = 5;
        this.canPull = true;                                                         
        this.attachedAnimator = this.gameObject.GetComponent<Animator>();
    }

    void OnCollisionEnter2D(Collision2D other)
    {                                                     
        if (other.gameObject.tag == "Meteorit")
        {
            health--;
            Destroy(other.gameObject);
            this.canPull = false;
            print("can pull");                  
            print(health);
            this.currentMeteorite = other.gameObject.GetComponent<HardMeteoritStateChanger>();
            PlayDestroyingAnimation();
            
        }

        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void PlayDestroyingAnimation()
    {
        print("Destroying");
        this.attachedAnimator.SetTrigger("Destroying");
    }

    public void DestroyMeteorite()
    {
        this.canPull = true;
    }



    void Update()
    {
        deltaTimeForPulling -= Time.deltaTime;
        if (deltaTimeForPulling <= 0 && currentMeteorite == null )
        {
            deltaTimeForPulling = 1f;
            meteorites = FindObjectsOfType<HardMeteoritStateChanger>().ToList();
        }

        if (meteorites != null && currentMeteorite == null && meteorites.Count != 0 )
        {
            HardMeteoritStateChanger closestMeteorite = meteorites.OrderBy(x => x.gameObject.transform.position.x - this.transform.position.x).First();
            this.currentMeteorite = closestMeteorite;
        }

        if (currentMeteorite != null && this.canPull)
        {                   
            float magnetMeteoriteDistance = (currentMeteorite.gameObject.transform.position.x - this.transform.position.x);
            currentMeteorite.transform.Translate(-new Vector3(magnetMeteoriteDistance, magnetMeteoriteDistance, 0) / 20);
        }

    }
}
