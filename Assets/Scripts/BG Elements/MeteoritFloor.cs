﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Cuby;

public class MeteoritFloor : MonoBehaviour
{
    private Vector2 postion;
    public static Transform floorPosition;
    bool isTrue = false;
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Meteorit" && CubyStaticData.IsAlive)
        {
            isTrue = true;
        }
    }

    void Start()
    {
        postion = GameObject.FindGameObjectWithTag("MeteoritFloor").transform.position;
        floorPosition = this.transform;
    }

    void Update()
    {
        if (isTrue)
        {
            postion.y += 0.2f;
            GameObject.FindGameObjectWithTag("MeteoritFloor").transform.position = postion;
            isTrue = false;
        }

        if (postion.y > -6)
        {
            Debug.Log("GameOver");
        }
    }
}
