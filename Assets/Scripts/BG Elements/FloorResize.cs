﻿using UnityEngine;
using System.Collections;

public class FloorResize : MonoBehaviour
{
    void Start()
    {
        Resize();
    }

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null)
        {
            return;
        }

        transform.localScale = new Vector3(1, 1, 1);
        float width = sr.sprite.bounds.size.x;
        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        Vector3 xWidth = transform.localScale;
        xWidth.x = worldScreenWidth / width + 0.04f;
        this.transform.localScale = xWidth;           
    }
}        
