﻿using Assets;
using Assets.Scripts.Bot;
using Assets.Scripts.Cuby;
using Assets.Scripts.Json;
using Assets.Scripts.UI.Settings;
using UnityEngine;
using Assets.Standard_Assets.CrossPlatformInput.Scripts;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CubyMovement : MonoBehaviour
{
    public static int currentStreak = 0;
    public AudioSource WalkingAudioSource;

    private Animator animator;
    private Rigidbody2D attachedRigidbody;
    private int buttonsMovement;
    private float playerControllerSensitivity;

    private new RectTransform transform;
    private float worldScreenHeight;
    private float worldScreenWidth;
    private int currentScore;
    public static Transform CurrTransform;
    private int currBullet = 0;
    private string currentPlayMode;
    Level currLevel = new Level();
    Player playerData = new Player();

    void Start()
    {
        this.playerControllerSensitivity = CurrentSettings.tiltSensitivity;
        this.worldScreenHeight = Camera.main.orthographicSize * 2f;
        this.worldScreenWidth = this.worldScreenHeight / Screen.height * Screen.width;
        this.animator = this.GetComponent<Animator>();
        this.attachedRigidbody = this.GetComponent<Rigidbody2D>();
        this.transform = FindObjectOfType<RectTransform>();
        Input.simulateMouseWithTouches = true;

        ExtractLevelSpecifics();

       // string currScene = SceneManager.GetActiveScene().name;
        CubyStaticData.Coins = 0;
        CurrTransform = this.transform;
        this.currentPlayMode = CurrentSettings.controller;

    }

    private void ExtractLevelSpecifics()
    {
        string player = PlayerPrefs.GetString("Cuby");
        
        string levelSpecifics = PlayerPrefs.GetString(SceneManager.GetActiveScene().name + "-Data");
        this.currLevel = JsonUtility.FromJson<Level>(levelSpecifics);
        this.playerData = JsonUtility.FromJson<Player>(player);
        this.playerData.regularBullets = this.currLevel.numberOfRegularBullets;
        BotStaticData.NumberOfRegularBullets = this.playerData.regularBullets;
    }

    void Update()
    {
        CheckForBtnMovement();
        CheckForTiltMovement();
        CheckForJoystickMovement();

        if (this.attachedRigidbody.transform.position.x >= this.worldScreenWidth / 2 - 1 && BotStaticData.Speed > 0)
        {
            CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;
        }
        else if (this.attachedRigidbody.transform.position.x <= 0 - this.worldScreenWidth / 2 + 0.8f && BotStaticData.Speed < 0)
        {
            CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;
        }

        this.attachedRigidbody.velocity = new Vector2(CubyStaticData.Speed, 0);
    }

    private void CheckForBtnMovement()
    {
        if (this.currentPlayMode == "Buttons")
        {
            if (this.buttonsMovement > 0)
            {
                this.MoveRight();
            }
            else if (this.buttonsMovement < 0)
            {
                this.MoveLeft();
            }
            else
            {
                this.SetToIdle();
            }
        }
    }

    public void RightButtonClicked()
    {
        this.buttonsMovement = 1;
    }

    public void LeftButtonClicked()
    {
        this.buttonsMovement = -1;
    }

    public void WalkButtonsReset()
    {
        this.buttonsMovement = 0;
    }

    private void CheckForTiltMovement()
    {
        if (this.currentPlayMode == "Tilt")
        {
            float tilt = Input.acceleration.x * 100;
            if (tilt > this.playerControllerSensitivity)
            {
                this.MoveRight();
            }
            else if (tilt < -this.playerControllerSensitivity)
            {
                this.MoveLeft();
            }
            else
            {
                this.SetToIdle();
            }
        }
    }

    private void CheckForJoystickMovement()
    {
        if (this.currentPlayMode == "Joystick")
        {
            float drag = CrossPlatformInputManager.GetAxis("Horizontal");
            if (drag > 0)
            {
                this.MoveRight();
            }
            else if (drag < 0)
            {
                this.MoveLeft();
            }
            else
            {
                this.SetToIdle();
            }
        }
    }

    public void SetToIdle()
    {
        if (CubyStaticData.IsAlive)
        {
            WalkingAudioSource.Stop();
            CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;
            this.animator.SetTrigger("IdleState");
        }
    }

    private void MoveLeft()
    {
        if (CubyStaticData.IsAlive)
        {
            if (!WalkingAudioSource.isPlaying)
            {
                WalkingAudioSource.Play();
            }

            CubyStaticData.Speed = -CubyStaticData.CUBY_NORMAL_SPEED;
            animator.SetTrigger("WalkingLeft");
        }
    }

    private void MoveRight()
    {
        if (CubyStaticData.IsAlive)
        {
            if (!WalkingAudioSource.isPlaying)
            {
                WalkingAudioSource.Play();
            }
            CubyStaticData.Speed = CubyStaticData.CUBY_NORMAL_SPEED;
            animator.SetTrigger("WalkingRight");
        }
    }
}
