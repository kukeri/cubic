﻿using Assets.Standard_Assets.CrossPlatformInput.Scripts.PlatformSpecific;

namespace Assets.Scripts.Cuby
{
    public static class CubyStaticData
    {
        public static readonly int CUBY_IDLE = 0;
        public static readonly int CUBY_NORMAL_SPEED = 6;

        public static int Health;
        public static int NumberOfRegularBullets;
        public static int numberOfHomingBullets;
        public static int numberOfRainPainBullets;
        public static int timeForForceField;

        public static bool EndOfGame;
        public static int Score;
        public static int CurrScore;

        public static int Coins;
        public static bool IsAlive;
        public static int Speed;
        public static int[] meteoritesDestroyed; // Use indexation with the enumeration meteorites 
    }
}
