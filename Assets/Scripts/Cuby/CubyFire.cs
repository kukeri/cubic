﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Cuby;
using Assets.Scripts.UI.InGame;
using Assets.Scripts.UI.Settings;

public class CubyFire : MonoBehaviour
{
    public Bullet normalBullet;
    public GameObject specialWeapon;
    public GameObject cubysForceField;
    public TimeManipulator timeManipulator;
    public AudioSource regularFireSound;

    private float offsetForPositionOfBulletFire = 0;
    private float offsetForPositionOfMineThrow = 0;
    private Rigidbody2D attachedRigidbody;
    private Animator attachedAnimator;

    void Start()
    {
        GetWantedComponents();
    }

    private void GetWantedComponents()
    {
        this.attachedRigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        this.attachedAnimator = this.gameObject.GetComponent<Animator>();
    }

    public void FireSpecialBullet()
    {
        if (CubyStaticData.IsAlive && !LevelStaticData.gameIsPaused)
        {
            if (specialWeapon.tag == "Bullet")
            {
                PlayFireAnimation();
                Instantiate(specialWeapon, new Vector2(attachedRigidbody.transform.position.x + offsetForPositionOfBulletFire, attachedRigidbody.transform.position.y + 1f), Quaternion.identity);

                CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;
            }
            else if (specialWeapon.tag == "Mine")
            {
                PlayFireAnimation();
                GameObject mine = (GameObject)Instantiate(specialWeapon, new Vector3(attachedRigidbody.transform.position.x + 2 * offsetForPositionOfBulletFire, attachedRigidbody.transform.position.y), Quaternion.identity);
                mine.GetComponent<Rigidbody2D>().AddForce(Vector2.right * offsetForPositionOfBulletFire * 500);
            }
            else if (specialWeapon.tag == "ForceField")
            {
                bool isActive = cubysForceField.activeSelf;
                cubysForceField.SetActive(!isActive);
            }
            else if (specialWeapon.tag == "TimeManipulator")
            {
                timeManipulator.ChangeMeteoriteTimeScale();
            }
        }
    }

    public void Fire()
    {
        if (CubyStaticData.IsAlive && !LevelStaticData.gameIsPaused)
        {
            if (CubyStaticData.NumberOfRegularBullets > 0)
            {
                this.PlayFireAnimation();
                Instantiate(this.normalBullet, new Vector2(this.attachedRigidbody.transform.position.x + this.offsetForPositionOfBulletFire, this.attachedRigidbody.transform.position.y + 1f), Quaternion.identity);
                CubyStaticData.NumberOfRegularBullets--;

                if (!CurrentSettings.isFXMuted)
                {
                    this.regularFireSound.Play();
                }
            }

            CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;

        }
    }

    private void PlayFireAnimation()
    {
        if (CubyStaticData.Speed < 0)
        {
            attachedAnimator.SetTrigger("FireLeft");
            this.offsetForPositionOfBulletFire = -0.3f;
        }
        else
        {
            attachedAnimator.SetTrigger("FireRight");
            this.offsetForPositionOfBulletFire = 0.3f;
        }

    }
}
