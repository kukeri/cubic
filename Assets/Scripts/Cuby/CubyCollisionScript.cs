﻿using UnityEngine;
using Assets.Scripts.Cuby;
using Assets.Scripts.Meteorites;

public class CubyCollisionScript : MonoBehaviour
{
    Animator animator;
                                       
    void Start()
    {
        this.animator = this.gameObject.GetComponent<Animator>();
    }    

    public void OnCollisionEnter2D(Collision2D other)
    {
        this.CheckForMeteoriteCollision(other);
        this.CheckForCoinCollision(other);
    }

    protected virtual void CheckForMeteoriteCollision(Collision2D other)
    {
        if (other.gameObject.tag == "Meteorit")
        {
            if (CubyStaticData.Health == 0)
            {
                SetToDying();
            }
        }
    }

    protected virtual void CheckForCoinCollision(Collision2D other)
    {
        if (other.gameObject.tag == "Coin")
        {
            CubyStaticData.Coins++;
            Destroy(other.gameObject);
        }
    }

    private void SetToDying()
    {
        MeteoriteStaticData.ResetMeteoritesDestroyed();
        animator.SetTrigger("DyingCubyState");
        CubyStaticData.IsAlive = false;
        CubyStaticData.Speed = CubyStaticData.CUBY_IDLE;
    }

}
