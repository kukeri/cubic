﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Cuby;

public class RegularBulletBonus : MonoBehaviour
{          
    private int regularBulletsGiven;

	void Start ()
	{
	    regularBulletsGiven = 5;
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        print(other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            CubyStaticData.NumberOfRegularBullets += regularBulletsGiven;
            Destroy(this.gameObject);
        }    
    }
    
}
