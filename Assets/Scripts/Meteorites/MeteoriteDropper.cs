﻿using System;
using Assets.Scripts;
using Assets.Scripts.Cuby;
using Assets.Scripts.Json;
using Assets.Scripts.Meteorites;
using Assets.Scripts.UI.InGame;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class MeteoriteDropper : MonoBehaviour
{
    public GameObject RegularMeteor;
    public GameObject FireMeteor;
    public GameObject IceMeteor;
    public GameObject HardMeteor;
    public GameObject SpiningMeteor;
    public GameObject endOfGame;

    private float width;

    private float regularMeteoritTimer;
    private float fireMeteoritTimer;
    private float iceMeteoritTimer;
    private float hardMeteoritTimer;
    private float spiningMeteoritTimer;

    private int numberOfRegularMeteorites;
    private int numberOfFireMeteorites;
    private int numberOfIceMeteorites;
    private int numberOfHardMeteorites;
    private int numberOfSpinningMeteorites;

    private int meteortiDropCount;
    private int totalMeteoritCount;
    private Level level;
    float timer;
    private float endOfGameTimer = 5f;

    void Start()
    {
        string currentLevelData = PlayerPrefs.GetString(SceneManager.GetActiveScene().name + "-Data");

        this.ExtractData(currentLevelData);
    }

    private void ExtractData(string currentLevelName)
    {
        this.level = JsonUtility.FromJson<Level>(currentLevelName);

        this.regularMeteoritTimer = this.level.regularMeteoritTimer;
        this.fireMeteoritTimer = this.level.fireMeteoritTimer;
        this.iceMeteoritTimer = this.level.iceMeteoritTimer;
        this.hardMeteoritTimer = this.level.hardMeteoritTimer;
        this.spiningMeteoritTimer = this.level.spiningMeteoritTimer;

        this.totalMeteoritCount = this.level.totalMeteoritCount;
        this.numberOfRegularMeteorites = this.level.numberOfRegularMeteorites;
        this.numberOfFireMeteorites = this.level.numberOfFireMeteorites;
        this.numberOfIceMeteorites = this.level.numberOfIceMeteorites;
        this.numberOfHardMeteorites = this.level.numberOfHardMeteorites;
        this.numberOfSpinningMeteorites = this.level.numberOfSpinningMeteorites;
        this.meteortiDropCount = this.level.meteoriteDropCount;
    }

    void Update()
    {
        if (this.EndOfLevel())
        {
            this.endOfGameTimer -= Time.deltaTime;

            LevelStaticData.LevelHasEnded = true;

            if (this.endOfGameTimer <= 0)
            {
                this.endOfGame.SetActive(true);
                MeteoriteStaticData.ResetMeteoritesDestroyed();
                CubyStaticData.EndOfGame = true;
            }
        }
        else
        {
            if (CubyStaticData.IsAlive)
            {
                this.TryToDropPossibleMeteorites();
            }
            else
            {
                
            }     
        }

    }

    private void TryToDropPossibleMeteorites()
    {
        this.TryDropCurrentMeteorite(this.RegularMeteor, ref regularMeteoritTimer, ref this.numberOfRegularMeteorites);

        if (this.regularMeteoritTimer <= 0)
        {
            this.regularMeteoritTimer = this.level.regularMeteoritTimer;
        }

        this.TryDropCurrentMeteorite(this.FireMeteor, ref fireMeteoritTimer, ref this.numberOfFireMeteorites);

        if (this.fireMeteoritTimer <= 0)
        {
            this.fireMeteoritTimer = this.level.fireMeteoritTimer;
        }

        this.TryDropCurrentMeteorite(this.IceMeteor, ref iceMeteoritTimer, ref this.numberOfIceMeteorites);
        if (this.iceMeteoritTimer <= 0)
        {
            this.iceMeteoritTimer = this.level.iceMeteoritTimer;
        }

        this.TryDropCurrentMeteorite(this.HardMeteor, ref hardMeteoritTimer, ref this.numberOfHardMeteorites);
        if (this.hardMeteoritTimer <= 0)
        {
            this.hardMeteoritTimer = this.level.hardMeteoritTimer;
        }

        this.TryDropCurrentMeteorite(this.SpiningMeteor, ref spiningMeteoritTimer, ref this.numberOfSpinningMeteorites);
        if (this.spiningMeteoritTimer <= 0)
        {
            this.spiningMeteoritTimer = this.level.spiningMeteoritTimer;
        }
    }

    private void TryDropCurrentMeteorite(GameObject meteorit, ref float levelTimer, ref int currentMeteoriteCount)
    {
        levelTimer -= Time.deltaTime;

        if (levelTimer <= 0 && currentMeteoriteCount != 0 && MeteoriteStaticData.totalMeteoritesDestroyed <= this.totalMeteoritCount)
        {
            currentMeteoriteCount--;
            this.DropMeteorite(meteorit);
        }
    }

    private void DropMeteorite(GameObject meteorit)
    {
        this.width = this.GetScreenSize();

        Instantiate(meteorit, new Vector2(Random.Range(-this.width + 1, this.width - 1), 8f),
            Quaternion.Euler(Vector3.zero));
    }

    bool EndOfLevel()
    {
        return this.numberOfFireMeteorites <= 0 && this.numberOfIceMeteorites <= 0 && this.numberOfRegularMeteorites <= 0 && this.numberOfHardMeteorites <= 0
            && MeteoriteStaticData.totalMeteoritesDestroyed >= this.totalMeteoritCount && this.numberOfRegularMeteorites <= 0;
    }

    float GetScreenSize()
    {
        float worldScreenHeight = Camera.main.orthographicSize;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        return worldScreenWidth;
    }
}

