﻿using UnityEngine;

namespace Assets.Scripts
{
    using Cuby;
    using Meteorites;

    class IceMeteoriteStateChanger : MeteoriteStateChanger
    {                   
        protected override void InitialiseObjectData()
        {
            base.InitialiseObjectData();
            this.health = 2;
            this.verticalForce = 4;
            this.horizontalForce = 0;
        }

        protected override void CheckForBulletColiision(Collider2D other)
        {
            if (other.gameObject.tag == "Bullet")
            {
                if (this.health == 1)
                {
                    this.animator.SetTrigger("LastBulletHit");
                    this.isCollidable.enabled = false;
                    this.health = 2;
                    CubyMovement.currentStreak++;
                    MeteoriteStaticData.totalMeteoritesDestroyed++;
                    CubyStaticData.CurrScore += this.AddScore();
                    Instantiate(this.coin, new Vector2(this.rigidbody.position.x, this.rigidbody.position.y), Quaternion.identity);

                }
                else
                {
                    this.health--;
                    this.animator.SetTrigger("FirstBulletHit");
                }
            }
            else if (other.gameObject.tag == "Floor" || other.gameObject.tag == "MeteoritFloor")
            {
                this.animator.SetTrigger("FloorHit");
            }
        }

        public override int AddScore()
        {
            return this.Score = 30;
        }
    }
}
