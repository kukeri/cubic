﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.Cuby;
using Assets.Scripts.Meteorites;

public class HardMeteoritStateChanger : MeteoriteStateChanger
{

    protected override void InitialiseObjectData()
    {
        base.InitialiseObjectData();
        this.health = 3;
    }

    protected override void CheckForBulletColiision(Collider2D bulletCollision)
    {
        if (bulletCollision.gameObject.tag == "Bullet")
        {
            if (this.health == 3)
            {
                this.animator.SetTrigger("FirstHit");
                this.health--;
            }
            else if (this.health == 2)
            {
                this.animator.SetTrigger("SecondHit");
                this.health--;
            }
            else if (this.health == 1)
            {
                this.animator.SetTrigger("Destroy");
                this.health--;
                MeteoriteStaticData.totalMeteoritesDestroyed++;
                Instantiate(coin, new Vector2(this.rigidbody.position.x, this.rigidbody.position.y), Quaternion.identity);

                CubyStaticData.CurrScore += this.AddScore();
            }
        }
    }

    public override int AddScore()
    {
        return this.Score = 40;
    }
}
