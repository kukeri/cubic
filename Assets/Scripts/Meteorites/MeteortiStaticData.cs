﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Assets.Standard_Assets.CrossPlatformInput.Scripts.PlatformSpecific;

namespace Assets.Scripts.Meteorites
{
    public static class MeteoriteStaticData
    {
        public static int totalMeteoritesDestroyed;
        public static int regularMeteoritesDestroyed;
        public static int iceMeteoritesDestroyed;
        public static int hardMeteoritesDestroyed;
        public static int fireMeteoritesDestroyed;
        public static int spinningMeteoritesDestroyed;
        public static float timeScale = 1f;

        public static void ResetMeteoritesDestroyed()
        {
            totalMeteoritesDestroyed = 0;
            regularMeteoritesDestroyed = 0;
            iceMeteoritesDestroyed = 0;
            hardMeteoritesDestroyed = 0;
            fireMeteoritesDestroyed = 0;
            spinningMeteoritesDestroyed = 0;
        }
    }
}
