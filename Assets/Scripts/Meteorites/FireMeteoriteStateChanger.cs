﻿using UnityEngine;

namespace Assets.Scripts
{
    using Cuby;
    using Meteorites;

    class FireMeteoriteStateChanger : MeteoriteStateChanger
    {

        protected override void InitialiseObjectData()
        {
            base.InitialiseObjectData();
            this.health = 1;
            this.verticalForce = 4.5f;
        }

        protected override void CheckForBulletColiision(Collider2D other)
        {
            if (other.gameObject.tag == "Bullet")
            {

                this.animator.SetTrigger("BulletHit");
                this.isCollidable.enabled = false;
                CubyMovement.currentStreak++;
                MeteoriteStaticData.totalMeteoritesDestroyed++;
                CubyStaticData.CurrScore += this.AddScore();
                Instantiate(coin, new Vector2(this.rigidbody.position.x, this.rigidbody.position.y), Quaternion.identity);
            }
        }

        public override int AddScore()
        {
            return this.Score = 20;
        }
    }
}

