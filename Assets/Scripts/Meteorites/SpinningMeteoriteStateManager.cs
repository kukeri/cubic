﻿using UnityEngine;
using Assets.Scripts;

public class SpinningMeteoriteStateManager : MeteoriteStateChanger
{
    private float spriteWidth;
    private bool rightSide;
    private bool leftSide;

    protected override void InitialiseObjectData()
    {
        base.InitialiseObjectData();
        this.spriteWidth = this.gameObject.transform.localScale.x / 2;
        this.horizontalForce = 5;
    }

    protected override void Update()
    {

        if (this.transform.position.x > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).x - this.spriteWidth && !this.leftSide)
        {
            Debug.Log("It's in");
            this.horizontalForce *= -1;
            this.leftSide = true;
            this.rightSide = false;
        }

        if (this.transform.position.x < Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f)).x * -1 + this.spriteWidth && !this.rightSide)
        {
            Debug.Log("It's in");

            this.horizontalForce *= -1;
            this.leftSide = false;
            this.rightSide = true;

        }


        base.Update();

    }

    public override int AddScore()
    {
        return this.Score = 50;
    }
}
