﻿using UnityEngine;
using UnityEngine.VR;

namespace Assets.Scripts
{
    using Cuby;
    using Meteorites;

    public class MeteoriteStateChanger : MonoBehaviour
    {
        public MeteoriteDropper manager;
        protected Animator animator;
        protected new Rigidbody2D rigidbody;
        protected CubyMovement player;
        protected CircleCollider2D isCollidable;
        protected int regularMeteoritScore = 10;

        public float verticalForce;
        public float horizontalForce;

        protected double health;
        public GameObject coin;

        protected int Score = 10;

        public virtual void Start()
        {
            GetWantedComponents();
            InitialiseObjectData();
        }

        protected virtual void InitialiseObjectData()
        {
            this.health = 1;
            this.verticalForce = 2.5f;
            this.horizontalForce = 0f;
        }

        protected virtual void GetWantedComponents()
        {
            this.rigidbody = this.GetComponent<Rigidbody2D>();
            this.animator = this.GetComponent<Animator>();
            this.isCollidable = this.GetComponentInChildren<CircleCollider2D>();
            GameObject gameControler = GameObject.FindWithTag("Player");
            this.player = gameControler.GetComponent<CubyMovement>();
        }

        protected virtual void Update()
        {
            this.rigidbody.velocity = new Vector2(-this.horizontalForce * MeteoriteStaticData.timeScale, -this.verticalForce * MeteoriteStaticData.timeScale);
        }

        public virtual void OnTriggerEnter2D(Collider2D other)
        {
            this.CheckForBulletColiision(other);
        }

        public void DestroyMeteorit()
        {
            Destroy(this.gameObject);
        }

        public virtual void OnCollisionEnter2D(Collision2D other)
        {
            this.CheckForFloorCollision(other);
            this.CheckForPlayerCollision(other);
            this.CheckForForceFieldCollision(other);
        }

        protected virtual void CheckForPlayerCollision(Collision2D other)
        {
            if (other.gameObject.tag == "Player")
            {
                CubyMovement.currentStreak = 0;
                Destroy(this.gameObject);
            }
        }

        protected virtual void CheckForFloorCollision(Collision2D other)
        {
            if (other.gameObject.tag == ("MeteoritFloor") || other.gameObject.tag == "Floor")
            {
                CubyStaticData.CurrScore -= this.Score / 2;

                if (CubyStaticData.CurrScore < 0)
                {
                    CubyStaticData.CurrScore = 0;
                }
                MeteoriteStaticData.totalMeteoritesDestroyed++;
                this.animator.SetTrigger("FloorHit");
            }
        }

        protected virtual void CheckForForceFieldCollision(Collision2D other)
        {
            if (other.gameObject.tag == "ForceField")
            {
                DestroyMeteorit();
            }
        }

        protected virtual void CheckForBulletColiision(Collider2D other)
        {
            if (other.gameObject.tag == "Bullet")
            {
                this.animator.SetTrigger("Bullet");
                Instantiate(this.coin, new Vector2(this.rigidbody.position.x, this.rigidbody.position.y), Quaternion.identity);
                this.isCollidable.enabled = false;
                MeteoriteStaticData.totalMeteoritesDestroyed++;
                CubyStaticData.CurrScore += this.AddScore();
            }
        }

        public virtual int AddScore()
        {
            return this.Score;
        }

    }
}
